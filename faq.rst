============
Workflow FAQ
============

.. |cbox| image:: ./images/checkboxes.png
.. |arch_viewer_emis| image:: ./images/archive_viewer_emis.png
.. |pub_time| image:: ./images/pub_time.png
.. |date_range| image:: ./images/date_range.PNG
.. |pcid| image:: ./images/pcid.PNG

1. How to download multiple feeds from DMP? (DMP)
-------------------------------------------------

| Firstly, we have to find the desired feeds in DMParchive viewer. Then we mark all feeds we would like to download. ( checkbox is on the right of the feed name )
|

|cbox|

|

| Now scroll to the bottom of the page and click on **Download**.
|

2. How to trace an article's origin from EMIS frontend? (Manual Tagger)
-----------------------------------------------------------------------

| *If the mystery article is html*:
|   Find the article you would like to investigate on EMIS frontend and click on it. Below the title there are three blue buttons - click on **edit article**. ( this will open the manual tagger )
| Now scroll down to the bottom of the panel. In another row of blue buttons, click on **archive viewer**. ( This will open the article's origin feed in DMP in a separate window )
|
| *If the mystery artcile is a binary file (pdf, xlsx, etc.)*
|   Open the publication containing the mystery article. Then click on **edit all articles**. Find the article title you are looking for in the new panel. Click on **edit**. ( small rectangular button
| just before the title ). Now on the bottom of the new panel, click on **archive viewer**. ( This will open the article's origin feed in DMP in a separate window )

|

|arch_viewer_emis|

|

3. How to change the publishing date of a binary file (article that is pdf, xlsx etc.) on EMIS? (Manual Tagger)
---------------------------------------------------------------------------------------------------------------

| Open the publication containing the mystery article. Then click on **edit all articles**. Find the article
| title you are looking for in the new panel. Click on **edit**. Adjust **publish time** boxes.

|

|pub_time|

|

4. How to get the PCID from a PUBCODE? (dataproc tools/frontend)
----------------------------------------------------------------
|
| In DPRM you search for the PUBCODE. Open the *pubs* page for the publication. In parentheses next to the PUB_CODE is the PCID.

|

|pcid|

|

5. How to obtain all the published articles for a given PCID in a given period of time? (dataproc tools/frontend)
-----------------------------------------------------------------------------------------------------------------

| Open the publication with the given PCID in EMIS. ( pcid here is actually the pubcode )
| example link:  https://www.emis.com/php/sources/index/pub?pcid=$example_pcid_here
| Then click on **edit all articles**. ( this will open the manual tagger ) Now click on **refine search**. From the
| dropdown menu under *Date Range* select *range* and put in your desired period of time.

|

|date_range|

|
6. How to create an issue link? (JIRA)
---------------------------------------
First of all - What is issue link? It is a reference between connected issues (tickets). If two tickets are connected, this can be shown and visualized by “issue link”.

- On the top of a certain ticket, right below ticket name there are 4 buttons. The 3rd one is Link Issue.
|

.. image:: ./images/6.1.jpg

|
- Clicking on it takes you below the “ticket body” In a section “**Linked Issues**”, where you select the type of link you want to create and the issue to which you want a link:
|

.. image:: ./images/6.2.jpg

|
•	When done with both fields the **Link button**, below to the right gets activated. Click it and it is done!
-	Some extra: to view a nice graph of all the linked tickets to the current one follow:

  - At the end of the page right before the comments start there is a field - "**Activity**". There you can see the tabs "Comments", "Work log", "History" and “Issue Links”.
  - Select “Issue Links” and voila
  - For more info see: https://marketplace.atlassian.com/apps/1216207/issue-links-viewer?hosting=cloud&tab=overview

|
7. How to fix wrongly logged time? (JIRA)
-----------------------------------------
•	Go on the chosen ticket page in Jira where you logged wrong working time.
•	At the end of the page right before the comments start there is a field - "Activity". There you can see the tabs "Comments", "Work log", "History" and “Issue Links”.
•	Click "Work log".  There is a history of all your logged work for the ticket. 
•	Each logged segment ends with an Edit link. 
|

.. image:: ./images/7.1.png

|
•	When Edit is pressed – a pop up appears. There you can edit the time, date, description etc.
|

.. image:: ./images/7.2.png

|
* for more info on Tickets handling: https://securities.jira.com/wiki/spaces/EDP/pages/139067560/JIRA+General+Guidelines+for+Resolving+tickets
|
8. How to reprocess multiple feeds? (DMP)
-----------------------------------------
Warning, before following those instructions make sure the review option is as desired and replicated in https://dprm.securities.com/#/processing/of the corresponding pub-code(s)/ related by assign rule(s) to the feeds. 
Now that we ensured the painlessness of the operation lets’ cut to the chase:
•	In https://dmpweb.securities.com/arc_viewer.html after selecting the desired search options we end up with a list of feeds under search results.

|

.. image:: ./images/8.1.png

|

•	Now every roll in the Search Results is a separate feed. 
•	At the end of the rolls there are checkboxes – we **check the boxes of the feeds** we want to reprocess and then hit the **“Reprocess” button** down-left.
•	If the dropdown “Reception agent” is on “Any” or “Reprocessed”, if we hit the “Go” button again we will see our reprocessed feeds listed in the Search Results. Their names will match the original feed’s name, but will be marked REPR under the column Rec. Agent.
•	If the dprm “review” option is “No” – those multiple reprocessed feeds will automatically be published directly in EMIS. 
 Else when “review” is “Yes”,
 
 -	we could check each reprocessed feed before publishing by clicking on the unpack process number and 
 -	then clicking on the PID number in the “Assigned Publications” table
 -	This will lead us to a preview of the processed articles.
|
9. How to use a locally generated index file? (DPRM-WDP)
--------------------------------------------------------
For starters, we navigate to the WDP for which we want to use a locally generated index:
•	From https://dprm.securities.com/ search box in the middle top we enter the respective Inbox. Then click the right search result.

|
|

.. image:: ./images/9.1.png

|
•	In the opened window there is a table named “Download Processes”
•	Next we click the Download ID or the “View” icon of the WDP of our choice.
|

.. image:: ./images/9.2.png

|
•	In the WDP View window there is a button Index file Upload (second orange button on the top)
|

.. image:: ./images/9.3.png

|
•	We browse for the desired index and upload it (it can be txt file) (left window on the pic below). 
Internally our file is renamed to a convention WDP_ID_ INDEX_timestamp (e.g. ENC_TEST_5_INDEX_201911061601).
 The window changes to ask if we want to put it in production (i.e. use it) (middle window on the pic below). When we click on “Put in production?” the window changes to inform for the swap of the previous index with our new one. (right window on the pic below)
|

.. image:: ./images/9.4.png

|
•	Thus we end up with uploaded custom index file used by the dprm platform
|
10. How to upload and use a mapping in the processing parser? (DPRM-Parser)
---------------------------------------------------------------------------
In the documentation there are two ways to access the uploader. 

The first method to upload a file:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

	In DPRM, in the **View Parser** page (e.g. something like https://dprm.securities.com/#/parser/**PUBCODE**/view/DMPDOC), on every field with little green icon at the top right, we can upload a mapping. 

	|

	.. image:: ./images/10.1.png

	|
	
	When we click on this icon we enter the **parser map view**, where we can choose a file to upload. The file can be a csv or xlsx/xls. First column must be keys, second - values. Keys must be unique.

	|

	.. image:: ./images/10.3.png

	|
	After a successful upload, you can **view** it or **set to production** by clicking the respective button. The white box letter after the view button changes from D to P, after your mapping is set to production.
	The mapping should be set to production in order to use it.	

|

The second method to upload a file:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

	We can directly reach the **Parser Map View** page if in DPRM we click on our name's dropdown menu in the top right of the page and select **Parser Map Search**
	
	|

	.. image:: ./images/10.4.png

	|
	
	When we enter the https://dprm.securities.com/#/parser_map/ we can search by pubcode and reach the same **parser map view** page as shown in the first method.
	
	|

Using the uploaded file:
^^^^^^^^^^^^^^^^^^^^^^^^

To access the uploaded mapping in the parser script we use:

# for a single TAG (1 property - for example INDU) mapping

my $indu_map = **getParserLocalMap('INDU')**; 

# in this case the $indu_map is a reference to a hash { key1 => value1, key2 => value2, etc }

|

# for a full mapping of the pub

my $local_map = getParserLocalMap();

# result: $local_map = { TAG1 => { key1 => value1, key2 => value2 },

# TAG2 => { key1 => value1, key2 => value2 }, # etc }

# where TAGs are DATE/TITL/BODY/RLTM/PROD/CMPY/LANG/INDU/SCOP/DTYP

|

*For more info please check the documentation file: DMPManual_version2019.pdf, chapter: 10.0 DPRM/DMP - Parser Local Map, page 102 on current version

|

11. What is the difference between publication and process?
------------------------------------------------------------
In different teams have different definition of process. For us, process is a set of all stages that the information goes through before it is published on EMIS. We have Download Process and Parsing Process which are part of the process cycle. For us, process is not only one process with one pid.

|
Every publication has publication code and publication name. For every publication we have one active Download Process and one Parsing Process.

|
12. In which cases we use EPS?
-----------------------------------------------
We use EPS when the information comes in a non-standard way. For example the information comes via email and in the feed we have file with links in it. Another example of using EPS is when we want to split a content of one PDF in several PDFs and vice versa - the contents of many PDFs in one file. In these cases we must use nonDMP 0.1 or Pandora.

|

13. Where is the EPS scripts?
------------------------------------------------
Your primary script is /inbox/<inbox>/bin/auto_run if it is on nonDMP 0.1 and it is /proj/inbox/bin/<inbox_id>/<pubcode_id>/auto_run if it is on Pandora. This gets called by sweep. Make sure it is executable. It must run within the max allowed execution time (currently 5 minutes). If you had any processing that you know for certain would take longer than this timeout, rename it to auto_run_long. This instructs sweep not to impose execution time limits on your script. You can have any other program in directory, but don't name them auto_run or auto_run_log.

|
Sweep is framework program for processing feeds arriving into /proj/reception/ftp/<inbox_id>/data/ dir from DMP. It runs from admin cron in a single instance every minutes.
It examines all dirs under /proj/reception/ftp/<inbox_id>/data/ owned by admin (important: all other dirs will be ignored!).

|

14. Can we see all the errors and where?
------------------------------------------------
Yes, we can see all the errors in DMP. That can be done in a few steps:

|
•	From the dropdown menu in DMP we must choose "Error logs"

|
.. image:: ./images/14_1.png
|

•	After that we will see a window like in the picture below (picture 16_2). There are some indoxes where we can add information about the inbox and the publication of the procces which we want to see the errors.

|
.. image:: ./images/14_2.png
|

15. Can we manually upload a feed and how?
------------------------------------------------
Yes, we can upload a feed in DMP. That can be done in a few steps:

|
•	From the dropdown menu in DMP you must choose "Feed uploader" (picture 15_1)

|
.. image:: ./images/15_1.png
|

•	In the inbox of Inbox we must write the name of the inbox of the publication in which we want to upload feed. After that we must click "Choose file" and choose a file from our files. NB: The names of the feed and the files in it must match the regular expressions in the assign rule to out publication. (picture 15_1)
|
.. image:: ./images/15_2.png
|



16. What is the AWS View tab used for?
--------------------------------------

| `AWS View <https://dmpweb.securities.com/aws.html>`_ is a place in which you can see information about our AWS services.
| You can observe that there are different tables with information about AWS server instances for formatting, reception, WDP and so on.
| An interesting, not so self-explanatory thing, that you can check out, is when you put your cursor over the numbers under **RMfeeds**, 
| you can see the count of feeds, of a certain inbox, that are on hold because the formatting servers are full of data, and are not 
| capable of handling more processes.

.. image:: images/awsview.png

|
|

17. By what criteria do we choose which ticket to start working on?
-------------------------------------------------------------------
| Every ticket has a **priority level**, by which we can see the level of urgency for it to be resolved. 
| When we look at the Issue Feed, in the `JIRA Issue Navigator <https://securities.jira.com/issues>`_, 
| we can see on the rightmost side of the feed, the priorities of the issues/tickets:

.. image:: images/issues_feed.png
|

There are 6 priority levels:


.. image:: images/priorities.png

|
|

18. What are industry codes used for?
-------------------------------------
| The industry code is used for mapping the type of the industries to certain numbers(codes).
| `EMIS Search Page <https://www.emis.com/php/search/search>`_  - here you can filter out articles by a lot of things, and one of those things is by *Industry*.
| Firstly, you'll have to click on the **More Filters** button, then click on the dropdown - **Industries**.
| There you go, now you can see how the industries are linked to a certain combination of digits.

|
|

19. How to match special symbols in the processing parser script if the current one doesn't work?
----------------------------------------------------------------------------------------------------
|
For example, let's take into consideration this case:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| The string that is at fault for our broken script is - '**cómoda**'.
| This string has to be removed from all articles, but it seems that fully hardcoding it doesn't work.
| *As if we have to be aware of something else?*
|
| Here is how to do it:
.. code:: c
   
   my $re_problematic_string = 'c[^<]+moda';

   # Substituting the string with an empty one.
   $body =~ s/$re_problematic_string//ig;

|

| Explanation:
| Here the trick is that you have to be aware that sometimes, when a certain encoding doesn't recognize a symbol (in this case - ó),
| it reads it as many symbols, none of which is this one.
| So what we do here is, instead of matching one symbol, we match one or more, so that even if the symbol breaks, when it is being read,
| our script will still substitute it.

|
|

20. Why is it necessary, in some cases, to change the encoding of the data in the processing parser?
----------------------------------------------------------------------------------------------------
| Sometimes it is necessary, because depending on the language of the text that is coming as an input,
| at the final stage of the processing, our framework expects it to have a certain encoding.
| For example if the text is in Bulgarian, at the final step, before publishing, the framework will expect,
| let's say, CP-1251. But sometimes the input encoding won't match with the one that the framework deduced.
| What happens then is, the final product will have broken symbols. Which is not cool, isn't it?
| So what we can do about it, in the Processing Parser Tab, is, if we know what the encoding of the text
| coming as an input is, before the publishing, we can convert it from `input encoding` to `expected encoding`.
| Here the expected encoding of the framework, becomes the one that you've specified.
.. image:: images/enc_conv.png

|

| Another way to do it, if you are SURE that the incoming text is in UTF-8, you can change the expected encoding
| to be UTF-8, from the Processing Tab.
.. image:: images/proc_utf8.png

Additional Question 1D: What happens when “clone process” is pressed? (DMP)
------------------------------------------------------------------------

Clone process is a button link in the DMP processing page. It is mainly used when we are testing and debugging a certain processing.

|

.. image:: ./images/1D.1.png

|

It is applied on the same feed and for the same pub-code as the original process it clones. It takes into account the changes in the scripts and changes in the interface options (as long as they are the replicated), made after the original process. When pressed it skips the assign rule and runs the processing scripts. It has its' own PID.

|

Additional Question 2D: How to create a ticket (aka issue)?
-----------------------------------

Tickets are known as issues in the Jira system. When visiting https://securities.jira.com/secure/Dashboard.jspa, we can create a new issue in a few ways:

1. by pressing the key C on the keyboard,
2. by clicking on the plus sign (+) on the left side-banner, (See arrows in the pic bellow)
3. by clicking on the **Create issue** link in the Quick links section in the bottom-right part of the page. (See arrows in the pic bellow)

|

.. image:: ./images/2D.1.png

|

If we follow options 1 or 2, a pop up window appears with all there is to be filled. Otherwise if we choose the 3rd option, we are navigated to a new page with step-by-step approach to creating an issue. For 1 and 2:

|

.. image:: ./images/2D.2.png

|

In the current case we are raising a ticket for a bug in the DPRM - so we choose as *project* EMIS Back Office (BO), *Issue Type* is Bug and so on. We have to accurately fill in the fields providing in depth info so that the issue is communicated rightly. Finally when we finish with the description we click on **Create**.

