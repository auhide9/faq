6. How to create an issue link? (JIRA)
---------------------------------------
First of all - What is issue link? It is a reference between connected issues (tickets). If two tickets are connected, this can be shown and visualized by “issue link”.

- On the top of a certain ticket, right below ticket name there are 4 buttons. The 3rd one is Link Issue.

.. image:: 6.1.jpg

- Clicking on it takes you below the “ticket body” In a section “**Linked Issues**”, where you select the type of link you want to create and the issue to which you want a link:

.. image:: 6.2.jpg

•	When done with both fields the **Link button**, below to the right gets activated. Click it and it is done!
•	Some extra: to view a nice graph of all the linked tickets to the current one follow:

 - At the end of the page right before the comments start there is a field - "**Activity**". There you can see the tabs "Comments", "Work log", "History" and “Issue Links”.
 - Select “Issue Links” and voila
 - For more info see: https://marketplace.atlassian.com/apps/1216207/issue-links-viewer?hosting=cloud&tab=overview

7. How to fix wrongly logged time? (JIRA)
-----------------------------------------
•	Go on the chosen ticket page in Jira where you logged wrong working time.
•	At the end of the page right before the comments start there is a field - "Activity". There you can see the tabs "Comments", "Work log", "History" and “Issue Links”.
•	Click "Work log".  There is a history of all your logged work for the ticket. 
•	Each logged segment ends with an Edit link. 

.. image:: 7.1.png





